<?php
require('Entrada.php');


class nuevaEntrada implements Entrada{
    private $id;
    private $Alumno;
    private $titulo;
    private $content;
    private $descripcion;
    private $date;


    function __construct($id,Alumno $Alumno,$titulo,$content,$descripcion,$date){
        $this->id = $id;
        $this->Alumno = $Alumno;
        $this->titulo = $titulo;
        $this->content = $content;
        $this->descripcion = $descripcion;
        $this->date = $date;
    }


    public function getId():int{
        return intval($this->id) ;
    }

    /** El autor de la entrada */
    public function getAlumno():Alumno{
        return $this->Alumno;
    }
    /** El título de la entrada */
    public function getTitulo():string{
        return $this->titulo;
    }
    /** El cotenido de la entrada */
    public function getContent():string{
        return $this->content;
    }
    /** El estracto que resume de la entrada */
    public function getDescripction():string{
        return $this->descripcion;
    }
    /** La fecha de la entrada */
    public function getDate(){
        return	$this->date;
    }
}


?>